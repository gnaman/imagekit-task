const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const cors = require("cors");
const path = require("path");
var MongoClient = require("mongodb").MongoClient;

var PORT = process.env.PORT || 8000;

var LOG_COLLECTION = "ip_logs";
var USERS_COLLECTION = "users";

var connectionString =
  'mongodb://' + process.env.MONGO_USR + ':' + process.env.MONGO_PWD + '@ds343217.mlab.com:43217/imagekit';

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

MongoClient.connect(connectionString, function(err, client, res) {
  console.log("Connected successfully to server", err);
  db = client.db("imagekit");

  app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/index.html"));
  });

  app.post("/register", async (req, res, next) => {
    const ip =
      req.headers["x-forwarded-for"] ||
      req.connection.remoteAddress.split(`:`).pop();
    console.log(ip);

    const userCredentials = {
      email: req.body.email,
      password: req.body.password,
      name: req.body.name
    };

    var nowDate = new Date();
    nowDate =
      nowDate.getFullYear() +
      "/" +
      (nowDate.getMonth() + 1) +
      "/" +
      nowDate.getDate();

    db.collection(LOG_COLLECTION).findOneAndUpdate(
      { ip: ip.toString(), date: nowDate },
      { $inc: { count: 1 } },
      (err, user) => {
        if (err) {
          next(err);
        } else if (!user.value) {
          db.collection(LOG_COLLECTION).insertOne(
            { ip: ip.toString(), date: nowDate, count: 1 },
            (err, user) => {
              if (err) next(err);
              else {
                registerUser(userCredentials)
                  .then(user => {
                    return res.send(user);
                  })
                  .catch(err => {
                    res.send(err.message);
                    console.log(err)
                    next(err);
                  });
              }
            }
          );
        } else {
        //   console.log(user);
          if (user.value.count > 3) {
            if (!req.body["g-recaptcha-response"]){
                res.send('Invalid Captcha');
                next(new Error("Invalid Captcha"));
            }
            else {
              var formData = {
                secret: process.env.RECAPTCHA_SECRET,
                response: req.body["g-recaptcha-response"]
              };
              request.post(
                "https://www.google.com/recaptcha/api/siteverify",
                { form: { ...formData } },
                (err, response) => {
                  if (err) {
                    console.log(err);
                    next(err);
                  } else if (response) {
                    response = JSON.parse(response.body);
                    console.log(response);
                    if (response.success) {
                        console.log(response);
                      registerUser(userCredentials)
                        .then(user => {
                          return res.send(user);
                        })
                        .catch(err => {
                          res.send(err.message);
                          next(err);
                        });
                    } else {
                      res.send('Invalid Captcha');
                      next(new Error("Bot boii"));
                    }
                  }
                }
              );
            }
          } else {
            console.log("user.value < 3");
            registerUser(userCredentials)
              .then(user => {
                return res.send(user);
              })
              .catch(err => {
                  res.send(err.message);
                next(err);
              });
          }
        }
      }
    );
  });

  app.get("/login", (req, res) => {
    console.log(req.connection.remoteAddress);

    const ip =
      req.headers["x-forwarded-for"] ||
      req.connection.remoteAddress.split(`:`).pop();

    // const nowDate = new Date();
    var nowDate = new Date();
    newDate =
      nowDate.getFullYear() +
      "/" +
      (nowDate.getMonth() + 1) +
      "/" +
      nowDate.getDate();

    db.collection(LOG_COLLECTION).findOne(
      { ip: ip.toString(), date: newDate },
      (err, user) => {
        if (err) next(err);
        else {
        //   console.log(user);
          return res.send({ ...user });
        }
      }
    );
  });

  const registerUser = async userCredentials => {
    var promise = new Promise((resolve, reject) => {
      db.collection(USERS_COLLECTION).findOne(
        { email: userCredentials.email },
        (err, user) => {
          if (err) {
            console.log(err);
            reject(err);
          } else {
            if (user) {
              const err = new Error("User is already registered");
              reject(err);
            } else {
              db.collection(USERS_COLLECTION).insertOne(
                { ...userCredentials },
                (err, user) => {
                  if (err) reject(err);
                  else {
                    resolve({ ...user.ops[0] });
                  }
                }
              );
            }
          }
        }
      );
    });

    return promise;
  };

  app.listen(PORT, () => {
    console.log("App listening on port " + PORT);
  });
});

module.exports = app;
